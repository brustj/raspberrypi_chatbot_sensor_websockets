const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io').listen(http);
const bodyParser = require('body-parser');
const onoff = require('onoff');
const Gpio = onoff.Gpio;

const led = new Gpio(17, 'out');
const pirSensor = new Gpio(27, 'in', 'both');

const port = 3000;
let motionFlag = false;

app.use( bodyParser.json() );
app.use( bodyParser.urlencoded({ extended: false }) );
app.get( '/', (req, res) => {
  return res.send( `Hello from BrustPi!` );
});

const setup = () => {
	led.writeSync(0);

	setInterval(() => loop(), 500);
};

const loop = () => {
	const motion = pirSensor.readSync();
	
	if (!motionFlag && motion) {
		motionFlag = true;

		led.writeSync(1);

		io.emit('rpi', `new user has been detected, begin experience`);
	} else if (motionFlag && !motion) {
		motionFlag = false;

		led.writeSync(0);

		io.emit('rpi', `user has left the area, end experience`);
	}
};

http.listen(port, () => {
	console.log( `Magic happens on port ${port}` );

	setup();
});